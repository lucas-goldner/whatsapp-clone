import 'package:flutter/material.dart';
import 'package:whatsapp_clone/services/auth.dart';
import 'package:whatsapp_clone/ui/chats.dart';
import 'package:whatsapp_clone/ui/stories.dart';
import 'package:whatsapp_clone/utils/material_color.dart';

void main() {
  runApp(const HomeScreen());
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Whatspp',
      theme: ThemeData(
        primarySwatch:
            createMaterialColor(const Color.fromRGBO(76, 203, 163, 1)),
      ),
      home: const MyHomePage(title: 'LuxApp'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final tabBarLabelStyle = const TextStyle(color: Colors.white, fontSize: 16);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
              centerTitle: false,
              title: Text(
                widget.title,
                style: const TextStyle(color: Colors.white),
                textAlign: TextAlign.start,
              ),
              actions: <Widget>[
                IconButton(
                  onPressed: () => {},
                  icon: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
                IconButton(
                    onPressed: () => {AuthService().logout()},
                    icon: const Icon(
                      Icons.more_vert,
                      color: Colors.white,
                    )),
              ],
              bottom: TabBar(
                isScrollable: true,
                tabs: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 16,
                    height: 50,
                    child: const Tab(
                        icon: Icon(
                      Icons.camera_alt,
                      color: Colors.white,
                    )),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 6,
                    height: 50,
                    child: Tab(
                        child: Text(
                      "Chats",
                      style: tabBarLabelStyle,
                    )),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 6,
                    height: 50,
                    child: Tab(
                        child: Text(
                      "Stories",
                      style: tabBarLabelStyle,
                    )),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 4,
                    height: 50,
                    child: Tab(
                        child: Text(
                      "Contacts",
                      style: tabBarLabelStyle,
                    )),
                  ),
                ],
                indicatorColor: Colors.white,
              )),
          body: const TabBarView(children: [
            Icon(Icons.camera_alt),
            ChatScreen(),
            StoryScreen(),
            Text("Contacts"),
          ]),
          floatingActionButton: FloatingActionButton(
            backgroundColor: const Color.fromRGBO(76, 203, 163, 1),
            foregroundColor: Colors.white,
            onPressed: () => {debugPrint("Test")},
            tooltip: 'Increment',
            child: const Icon(
              Icons.chat_sharp,
            ),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }
}
