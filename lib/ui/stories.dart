import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatsapp_clone/cubits/counter.dart';

void main() {
  runApp(const StoryScreen());
}

class StoryScreen extends StatefulWidget {
  const StoryScreen({Key? key}) : super(key: key);

  @override
  _Storyscreen createState() => _Storyscreen();
}

class _Storyscreen extends State {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => CounterCubit(),
        child: BlocBuilder<CounterCubit, int>(builder: (context, state) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Current count: $state'),
              IconButton(
                icon: const Icon(Icons.plus_one_rounded),
                onPressed: () => context.read<CounterCubit>().increment(),
              ),
              IconButton(
                icon: const Icon(Icons.exposure_minus_1_rounded),
                onPressed: () => context.read<CounterCubit>().decrement(),
              )
            ],
          );
        }));
  }
}
