import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:whatsapp_clone/services/auth.dart';

void main() {
  runApp(RegisterScreen());
}

class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key? key}) : super(key: key);
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  void registerHere() => AuthService().registerUser(emailController.text,
      passwordController.text, confirmPasswordController.text);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
          Lottie.asset(
            'assets/register.json',
            repeat: true,
            reverse: false,
            animate: true,
            height: 300,
          ),
          Form(
              child: Column(
            children: <Widget>[
              SizedBox(
                  width: MediaQuery.of(context).size.width / 1.3,
                  child: TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(hintText: "Email"),
                  )),
              SizedBox(
                  width: MediaQuery.of(context).size.width / 1.3,
                  child: TextFormField(
                    controller: passwordController,
                    decoration: const InputDecoration(hintText: "Password"),
                  )),
              SizedBox(
                  width: MediaQuery.of(context).size.width / 1.3,
                  child: TextFormField(
                    controller: confirmPasswordController,
                    decoration:
                        const InputDecoration(hintText: "Confirm Password"),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width / 1.3,
                  height: 50,
                  margin: const EdgeInsets.only(top: 10),
                  child: OutlinedButton(
                      onPressed: () => {registerHere()},
                      child: const Text("Register")))
            ],
          )),
        ])));
  }
}
