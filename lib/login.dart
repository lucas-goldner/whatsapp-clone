import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:whatsapp_clone/services/auth.dart';
import 'package:whatsapp_clone/ui/register.dart';

void main() {
  runApp(const LoginScreen());
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();
    return firebaseApp;
  }

  final emailController = TextEditingController();
  final passwortController = TextEditingController();
  void loginNow() =>
      AuthService().loginUser(emailController.text, passwortController.text);

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwortController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _initializeFirebase(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
                body: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                  Lottie.asset(
                    'assets/login.json',
                    repeat: false,
                    reverse: false,
                    animate: true,
                  ),
                  Form(
                      child: Column(
                    children: <Widget>[
                      SizedBox(
                          width: MediaQuery.of(context).size.width / 1.3,
                          child: TextFormField(
                            controller: emailController,
                            decoration:
                                const InputDecoration(hintText: "Email"),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width / 1.3,
                          child: TextFormField(
                            controller: passwortController,
                            decoration:
                                const InputDecoration(hintText: "Password"),
                          )),
                      Container(
                        margin: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                                onPressed: () => {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RegisterScreen()))
                                    },
                                child: const Text("Register")),
                            TextButton(
                                onPressed: () => {loginNow()},
                                child: const Text("Login"))
                          ],
                        ),
                      )
                    ],
                  )),
                ])));
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
